package Trendyol;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestCase {
	public WebDriver driver;
	
	@BeforeClass
	public void setUp() {

		System.out.println("*******************");
		System.out.println("launching chrome browser");
		String exePath = "C:\\Users\\buse\\Desktop\\Server\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exePath);
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}
	
	@Test(priority = 0)
	public void goToTrendyol() {
		driver.navigate().to("https://www.trendyol.com/");
		String strPageTitle = driver.getTitle();
		System.out.println("Page title: - "+strPageTitle);
		Assert.assertTrue(strPageTitle.contains("Trendyol"), "Page title doesn't match");
		if(driver.findElement(By.xpath("//div[@id='genderPopup']/div")).isDisplayed() == true) {
			driver.findElement(By.xpath("//div[@id='genderPopup']/div/div/a/span/img")).click();
		}
	}
	
	@Test(priority = 1)
	public void Login() throws InterruptedException {
		driver.findElement(By.xpath("(//i[@id='logo-icon'])[2]")).click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.id("loginSubmit")));
	    driver.findElement(By.id("email")).click();
	    driver.findElement(By.id("email")).clear();
	    driver.findElement(By.id("email")).sendKeys("work.buse2@gmail.com");
	    driver.findElement(By.id("password")).clear();
	    driver.findElement(By.id("password")).sendKeys("buse.elli50");
	    driver.findElement(By.id("loginSubmit")).click();
	    Thread.sleep(2000);
	    String link = "https://www.trendyol.com/Butik/Liste/Kad%C4%B1n?e=login";
	    String url = driver.getCurrentUrl();
		Assert.assertEquals(url,link);
	}
	
	@Test(priority=2)
	public void FindImages() {
		
		
		
			driver.findElement(By.xpath("//div[3]/div/div/ul/li/a")).click();
		    //Find total No of images on page and print In console
			List<WebElement> total_images = driver.findElements(By.tagName("img"));
			System.out.println("Total Number of images found on page = " + total_images.size());
			boolean isValid = false;
		    for (int i = 0; i < total_images.size(); i++) {
		    String url = total_images.get(i).getAttribute("src");
		    
		    if (url != null) {

		        //Call getResponseCode function for each URL to check response code.
		        isValid = getResponseCode(url);

		        //Print message based on value of isValid which Is returned by getResponseCode function.
		        if (isValid) {
		         
		        } else {
		         System.out.println("Broken image ------> " + url);
		         System.out.println("----------XXXX-----------XXXX----------XXXX-----------XXXX----------");
		         System.out.println();
		        }
		       
		        continue;
		       }
		    }
		      
		
		
	}
	
	@Test(priority=3)
	public void goToAllTab() {
		Random rnd = new Random();
		int y;
		for(y=0; y<10;y++) {
			
			int tab = rnd.nextInt(9);
			driver.findElement(By.xpath("//div[3]/div/div/ul/li["+(tab+1)+"]/a")).click();
		    //Find total No of images on page and print In console
			List<WebElement> total_images = driver.findElements(By.tagName("img"));
			System.out.println("Total Number of images found on page = " + total_images.size());
			boolean isValid = false;
		    for (int i = 0; i < total_images.size(); i++) {
		    String url = total_images.get(i).getAttribute("src");
		    
		    if (url != null) {

		        //Call getResponseCode function for each URL to check response code.
		        isValid = getResponseCode(url);

		        //Print message based on value of isValid which Is returned by getResponseCode function.
		        if (isValid) {
		         
		        } else {
		         System.out.println("Broken image ------> " + url);
		         System.out.println("----------XXXX-----------XXXX----------XXXX-----------XXXX----------");
		         System.out.println();
		        }
		       
		        continue;
		       }
		    }
		      
		}
	  }
	@Test(priority=4)
	public void goToAnyBoutique() throws InterruptedException {
		
		Random rnd = new Random();
		int y;
		int tab = rnd.nextInt(9);
		driver.findElement(By.xpath("//div[3]/div/div/ul/li["+(tab+1)+"]/a")).click();
		driver.findElement(By.xpath("//div[@id='dynamic-boutiques']/div/div/div/div[2]/div/a")).click();
		driver.findElement(By.cssSelector("div.checkbox")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@id='productsContainer']/div/div/a/div[3]/div/div/img")).click();
		String title = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[2]/div[2]/div/div[1]/div/div[1]/h1/span[1]/a")).getText();
		System.out.println(title);
		driver.findElement(By.xpath("//div[@id='addToBasketButton']/span")).click();
		Boolean z = driver.findElement(By.xpath("//button[@type='button']")).isDisplayed();
		if(z == true) {
			WebDriverWait wait2 = new WebDriverWait(driver, 10);
			WebElement element2 = wait2.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"mCSB_1\"]/div[1]/li[2]/a/span[1]")));
			driver.findElement(By.xpath("//*[@id=\"mCSB_1\"]/div[1]/li[2]/a/span[1]")).click();
	  }

		WebDriverWait wait3 = new WebDriverWait(driver, 10);
		WebElement element3 = wait3.until(ExpectedConditions.elementToBeClickable(By.linkText("Sepete Git")));
		driver.findElement(By.linkText("Sepete Git")).click();
		Thread.sleep(2000);
		String title2 = driver.findElement(By.xpath("/html/body/section/div[1]/div/div[2]/section/section[1]/div[2]/div/ul/li/div[2]/div[1]/a/span[1]")).getText();
		System.out.println(title2);
		Assert.assertEquals(title2, title);
		driver.findElement(By.linkText("Kaldır")).click();
		WebDriverWait wait4 = new WebDriverWait(driver, 10);
		WebElement element4 = wait4.until(ExpectedConditions.elementToBeClickable(By.linkText("Sil")));
		driver.findElement(By.linkText("Sil")).click();
		Thread.sleep(2000);
		Assert.assertFalse(driver.findElement(By.xpath("//section[@id='basketContent']/div[2]")).isDisplayed());
		
	}
	
		
	
	
  private boolean getResponseCode(String url) {
		// TODO Auto-generated method stub
		return true;
	}
@BeforeMethod
  public void beforeMethod() {
  }

  @AfterMethod
  public void afterMethod() {
  }

}
